class Product
  def initialize(params)
    @title = params[:title]
    @price = params[:price]
    @image = params[:image]
    @delivery_time = params[:delivery_time]
    @product_code = params[:product_code]
  end
  attr_accessor :title, :price, :image, :delivery_time, :product_code
end

