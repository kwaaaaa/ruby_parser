require_relative 'page_loader'
require_relative 'all_pages_links'
require_relative 'product_pages_loader'
require_relative 'product_pages_parser'
require_relative 'csv_writer'
require_relative 'product'

require 'rubygems'
require 'bundler/setup'

require 'pry'

class TaskRuby
  def initialize(url, file)
    raise ArgumentError.new("Not valid url") if url_invalid?(url)
    @url = url
    @file = file
  end
  def main
    raw_page = PageLoader.new(@url).load_page
    all_links = AllPagesLinks.new(raw_page, @url).create_links
    raw_product_pages = ProductPagesLoader.new(all_links).load_pages
    parse_product_pages = ProductPagesParser.new(raw_product_pages).parse_pages
    CsvWriter.new(parse_product_pages, @file).save_csv
  rescue Exception => e
    puts e.message
  end

  def url_invalid?(url)
    url =~ /\A#{URI::regexp}\z/ ? false : true
  end

end




