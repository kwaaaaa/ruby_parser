class ProductPagesLoader
  def initialize(multi_links)
    @multi_links = multi_links
  end

  def load_pages
    @multi_links.map { |el| PageLoader.new(el.to_s).load_page }
  end
end