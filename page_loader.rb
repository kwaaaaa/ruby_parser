require 'curb'

class PageLoader
  def initialize(url)
    @url = url
  end

  def load_page
    c = Curl.get(@url)
    if c.status != '200 OK' && @url.include?("http://")
        url = @url.dup
        url.gsub!("http://", "https://")
        PageLoader.new(url).load_page
    else
      c.body_str
    end
  end
end