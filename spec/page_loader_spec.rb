require 'spec_helper'
require 'curl'

describe PageLoader do
  describe '.load_page' do
    describe 'with url without https' do
      let(:page_loader) { FactoryGirl.build(:page_loader, url: 'http://www.test.com') }
      it 'tries to creates a valid link' do
        expect(page_loader.load_page).to include('!DOCTYPE html PUBLIC')
      end
    end
    describe 'with valid url' do
      let(:page_loader) { FactoryGirl.build(:page_loader, url: 'https://www.test.com') }
      it 'returns string' do
        expect(page_loader.load_page.class.name).to eq "String"
      end
      it 'contains valid html' do
        expect(page_loader.load_page).to include('!DOCTYPE html PUBLIC')
      end
    end
  end
end
