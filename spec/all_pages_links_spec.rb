require 'spec_helper'
require 'nokogiri'

describe AllPagesLinks do
  describe '.create_links' do
    describe 'with bnt next' do
      raw_pages_with_btn_next = IO.read(Dir.pwd + "/spec/raw_pages_with_btn_next.html")
      url = 'https://www.viovet.co.uk/Pet_Foods_Diets-Dogs-Hills_Pet_Nutrition-Hills_Prescription_Diets/c233_234_2678_93/category.html'
      let(:all_pages_links) { FactoryGirl.build(:all_pages_links, raw_page: raw_pages_with_btn_next, url: url) }

      it 'returns array' do
        expect(all_pages_links.create_links.class.name).to eq "Array"
      end

      it 'contains product links' do
        expect(all_pages_links.create_links.first).to eq "https://www.viovet.co.uk/Hills_Prescription_Diet_zd_Dog_Food/c107/"
      end

      it 'returns rigth element count' do
        expect(all_pages_links.create_links.count).to eq 6
      end

    end
    describe 'without bnt next' do
      raw_pages_without_btn_next = IO.read(Dir.pwd + "/spec/raw_pages_without_btn_next.html")
      url = 'https://www.viovet.co.uk/Pet_Foods_Diets-Dogs-Hills_Pet_Nutrition-Hills_Prescription_Diets/c233_234_2678_93/category.html'
      let(:all_pages_links) { FactoryGirl.build(:all_pages_links, raw_page: raw_pages_without_btn_next, url: url) }

      it 'returns array' do
        expect(all_pages_links.create_links.class.name).to eq "Array"
      end

      it 'contains product links' do
        expect(all_pages_links.create_links.first).to eq "https://www.viovet.co.uk/Hills_Prescription_Diet_zd_Dog_Food/c107/"
      end

      it 'returns rigth element count' do
        expect(all_pages_links.create_links.count).to eq 2
      end
    end
  end
end
