require 'spec_helper'

describe CsvWriter do
  describe '.save_csv' do
    parsed_page = [FactoryGirl.build(:product), FactoryGirl.build(:product)]
    file = 'test'
    let(:csv_writer) { FactoryGirl.build(:csv_writer, parsed_page: parsed_page, file: file) }

    it 'returns string' do
      expect(csv_writer.save_csv.class.name).to eq "String"
    end

    it 'contains file name' do
      expect(csv_writer.save_csv).to eq (file + ".csv created")
    end

    it 'creates file' do
      csv_writer.save_csv
      expect(true).to eq File.exists?(file + '.csv')
    end
  end
end