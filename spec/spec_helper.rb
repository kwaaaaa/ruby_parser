require_relative '../page_loader'
require_relative '../all_pages_links'
require_relative '../product_pages_loader'
require_relative '../product_pages_parser'
require_relative '../csv_writer'
require_relative '../product'
require_relative '../task_ruby'


require 'factory_girl'
require 'pry'
RSpec.configure do |config|
  config.color = true
end

FactoryGirl.find_definitions

