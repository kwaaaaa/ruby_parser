require 'spec_helper'

describe Product do
  let(:product) { FactoryGirl.build(:product) }
  subject {product}
  it { should respond_to(:title) }
  it { should respond_to(:price) }
  it { should respond_to(:image) }
  it { should respond_to(:delivery_time) }
  it { should respond_to(:product_code) }
end




