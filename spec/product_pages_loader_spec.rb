require 'spec_helper'
  require 'curl'

  describe ProductPagesLoader do
    describe '.load_page' do
      multi_links = ["https://www.viovet.co.uk/Hills_Prescription_Diet_zd_Dog_Food/c107/"]
      let(:product_pages_loader) { FactoryGirl.build(:product_pages_loader, multi_links: multi_links) }
      it 'returns array' do
        expect(product_pages_loader.load_pages.class.name).to eq "Array"
      end
      it 'contains product pages' do
        expect(product_pages_loader.load_pages.first.to_s).to include('!DOCTYPE html')
      end
    end
  end