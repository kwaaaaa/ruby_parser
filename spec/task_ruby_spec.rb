require 'spec_helper'

describe TaskRuby do

  valid_url = 'https://www.viovet.co.uk/Pet_Foods_Diets-Dogs-Hills_Pet_Nutrition-Hills_Prescription_Diets/c233_234_2678_93/category.html'
  invalid_url = 'www.viovet.co.uk/Pet_Foods_Diets-Dogs-Hills_Pet_Nutrition-Hills_Prescription_Diets/c233_234_2678_93/category.html'
  file = 'products'
  let(:task_ruby) { FactoryGirl.build(:task_ruby, url: valid_url, file: file) }

  describe '.main' do
    subject { task_ruby.main }
    it 'returns string' do
      expect(subject.class.name).to eq "String"
    end

    it 'returns file name' do
      expect(subject).to eq (file + ".csv created")
    end
  end

  describe '.url_invalid?' do
    describe 'with valid url' do
      it 'returns false' do
        expect(task_ruby.url_invalid?(valid_url)).to eq false
      end
    end
    describe 'with invalid url' do
      it 'returns true' do
        expect(task_ruby.url_invalid?(invalid_url)).to eq true
      end
    end
  end

  describe ".new with invalid url" do
    subject {TaskRuby.new(invalid_url, 'file') }
    it "raise exception" do
      expect { subject }.to raise_exception
    end
  end

end