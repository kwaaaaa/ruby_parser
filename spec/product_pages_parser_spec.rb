require 'spec_helper'

describe ProductPagesParser do
  describe '.parse_pages' do
    raw_product_pages = []
    raw_product_pages << IO.read(Dir.pwd + "/spec/raw_pages_with_btn_next.html")
    let(:product_pages_parser) { FactoryGirl.build(:product_pages_parser, raw_product_pages: raw_product_pages) }
    it 'returns array' do
      expect(product_pages_parser.parse_pages.class.name).to eq "Array"
    end
    it 'contains product class' do
      expect(product_pages_parser.parse_pages.first.class.name).to eq "Product"
    end
    it 'contains titles' do
      expect(product_pages_parser.parse_pages.first.title).to include("Dry » 5kg Bag")
    end
    it 'contains prices' do
      expect(product_pages_parser.parse_pages.first.price).to include("37.80")
    end
  end
end