FactoryGirl.define do
  factory :page_loader do
    url 'url'
    initialize_with { new(url) }
  end
  factory :all_pages_links do
    raw_page 'raw'
    url 'url'
    initialize_with { new(raw_page, url) }
  end
  factory :product_pages_loader do
    multi_links 'multi_links'
    initialize_with { new(multi_links) }
  end

  factory :product do
    title 'title'
    price 'price'
    image 'image'
    delivery_time 'delivery_time'
    product_code 'product_code'
    initialize_with { new(
        title: title,
        price: price,
        image: image,
        delivery_time: delivery_time,
        product_code: product_code) }
  end

  factory :product_pages_parser do
    raw_product_pages 'raw_product_pages'
    initialize_with { new(raw_product_pages) }
  end

  factory :csv_writer do
    parsed_page 'parsed_page'
    file 'file'
    initialize_with { new(parsed_page, file) }
  end

  factory :task_ruby do
    url 'url'
    file 'file'
    initialize_with { new(url, file) }
  end
end


