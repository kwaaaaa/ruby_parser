require 'nokogiri'

class ProductPagesParser
  def initialize(raw_pages)
    @raw_pages = raw_pages
  end

  def parse_pages
    @raw_pages.map { |raw_page| get_products(raw_page) }.flatten
  end

  def get_products(raw_page)
    main_title = get_main_title(raw_page)
    Nokogiri::HTML(raw_page).xpath(".//ul[@id='product_listing']/li[@itemprop='offers']").map do |li|
      Product.new(title: main_title + get_title(li),
                  price: get_price(li),
                  image: get_image(li, raw_page),
                  delivery_time: get_delivery_time(li),
                  product_code: get_product_code(li))
    end
  end

  def get_main_title(raw_page)
    Nokogiri::HTML(raw_page).at("//h1[@itemprop = 'name']").text.delete("\n\t").to_s + " - "
  end

  def get_title(li)
    Nokogiri::HTML(li.to_s).at("//div[@itemprop = 'name']").text.delete("\n\t")
  end

  def get_price(li)
    Nokogiri::HTML(li.to_s).at("//span[@itemprop = 'price']").children.text
  end

  def get_image(li, raw_page)
    if not_contains_image?(li)
      get_main_image(raw_page)
    else
      get_self_image(li)
    end
  end

  def get_self_image(li)
    "http:#{Nokogiri::HTML(li.to_s).at("//a[@itemprop = 'image']/@href").to_s}"
  end

  def get_main_image(raw_page)
    "http:#{Nokogiri::HTML(raw_page).at("//img[@class='_img_zoom']/@src").value.to_s}"
  end

  def get_delivery_time(li)
    Nokogiri::HTML(li.to_s).at("//strong[@class='stock in-stock' or @class='stock out-stock']").text.delete("\n\t")
  end

  def get_product_code(li)
    Nokogiri::HTML(li.to_s).at("//strong[@itemprop='sku']").children.text
  end

  def not_contains_image?(li)
    Nokogiri::HTML(li.to_s).at("//a[@itemprop = 'image']/@href").nil?
  end

end

