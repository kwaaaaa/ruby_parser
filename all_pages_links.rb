require 'uri'
require 'nokogiri'

class AllPagesLinks
  def initialize(raw_page, url)
    @doc = Nokogiri::HTML(raw_page)
    @url = url
    @host = URI.parse(url).host
  end

  def create_links
    links = @doc.xpath(".//a[@class='call_to_action _secondary _medium view_details']").map { |el| 'https://' + @host + el["href"]}
    if contains_next_page?
      links += get_next unless get_next.nil?
    end
    links
  end

  def contains_next_page?
    @doc.xpath(".//a[@class='next']/@href").size != 0
  end

  def get_next
    next_page_url = 'https://' + @host + @doc.xpath(".//a[@class='next']/@href").to_s
    next_raw_page = PageLoader.new(next_page_url).load_page
    AllPagesLinks.new(next_raw_page, @url).create_links
  end
  
end