require 'csv'

class CsvWriter
  def initialize(parsed_page, file)
    @parsed_page = parsed_page
    @file = file
  end

  def save_csv
    @file += ".csv" unless @file.include?(".csv")
    CSV.open(@file, "w") do |csv|
      @parsed_page.map.with_index do |product, index|
        csv << ["#{index + 1})"]
        csv << ["title: #{product.title}"]
        csv << ["price: #{product.price}"]
        csv << ["image: #{product.image}"]
        csv << ["delivery time: #{product.delivery_time}"]
        csv << ["product code: #{product.product_code}"]
      end
    end
    if File.file?(@file)
      puts "#{@file} created"
      "#{@file} created"
    end
  end
end